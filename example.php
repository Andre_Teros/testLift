<?php

require 'vendor/autoload.php';

try {
    $l = new Lift(33, 12, Lift::DIRECT_UP);

    $l->call(14, Lift::DIRECT_DOWN, 4)
        ->call(5, Lift::DIRECT_UP, 8)
        ->call(9, Lift::DIRECT_DOWN, 1)
        ->call(10, Lift::DIRECT_DOWN, 9)
        ->call(20, Lift::DIRECT_DOWN, 1)
        ->call(15, Lift::DIRECT_UP, 30)
        ->run();
} catch (\RuntimeException $exception) {
    echo $exception->getMessage();
}

echo $l->showLastTrip();
