<?php
declare(strict_types=1);

class Lift
{
    public const DIRECT_UP = 1;
    public const DIRECT_DOWN = -1;

    private $storeys;
    private $currentFloor;
    private $priorityDirection;
    private $currentDirect;
    private $calls = [];

    private $lastTrip;

    /**
     * Lift constructor.
     *
     * @param int $storeys number of storeys in building
     * @param int $currentFloor what floor is the lift on
     * @param int $priorityDirection which direction will lift move first
     *
     * @throws RuntimeException
     */
    public function __construct(int $storeys, int $currentFloor = 1, int $priorityDirection = self::DIRECT_DOWN)
    {
        $this->validatePrimaryData($storeys, $currentFloor);
        $this->storeys = $storeys;
        $this->currentFloor = $currentFloor;
        $this->priorityDirection = $priorityDirection;
    }

    /**
     * Method simulate pushing lift call button
     *
     * @param int $onFloor on which floor is the lift called
     * @param int $direction in which direction
     * @param int $toFloor which floor do you need to go to
     *
     * @return Lift
     *
     * @throws RuntimeException
     */
    public function call(int $onFloor, int $direction, int $toFloor): Lift
    {
        $this->validateCallData($onFloor, $direction, $toFloor);
        $this->calls[$direction][] = $onFloor;
        $this->calls[$direction][] = $toFloor;

        return $this;
    }

    /**
     * Method simulate lift movement
     */
    public function run(): void
    {
        $this->lastTrip = [];

        foreach ([$this->priorityDirection, -$this->priorityDirection] as $direction) {
            if (!empty($this->calls[$direction])) {
                $this->currentDirect = $direction;
                $this->move($this->calls[$direction]);
            }
        }
    }

    /**
     * Shows description of last lift trip
     *
     * @return string
     */
    public function showLastTrip(): string
    {
        return implode('; ', $this->lastTrip);
    }

    /**
     * @throws RuntimeException
     */
    private function validatePrimaryData(int $storeys, int $currentFloor)
    {
        if ($storeys < 2) {
            throw new \RuntimeException('Number of storeys must be more then 1');
        }

        if ($currentFloor > $storeys) {
            throw new \RuntimeException('Current floor cannot be greater then number of storeys');
        }
    }

    /**
     * @throws RuntimeException
     */
    private function validateCallData(int $onFloor, int $direct, int $toFloor)
    {
        if ($onFloor < 1 || $onFloor > $this->storeys) {
            throw new \RuntimeException("Wrong floor. Must be between 1 and {$this->storeys}");
        }

        if (($onFloor === 1 && $direct === self::DIRECT_DOWN)
            || ($onFloor === $this->storeys && $direct === self::DIRECT_UP)) {
            throw new \RuntimeException('Cannot move in this direction');
        }

        if (($onFloor < $toFloor && $direct === self::DIRECT_DOWN)
            || ($onFloor > $toFloor && $direct === self::DIRECT_UP)) {
            throw new \RuntimeException('Cannot move in this direction');
        }
    }

    private function move($floorList)
    {
        if ($this->currentDirect === self::DIRECT_DOWN) {
            rsort($floorList);
        } else {
            sort($floorList);
        }

        $floorList = array_unique($floorList);

        $start = array_shift($floorList);
        $end = array_pop($floorList);

        $this->lastTrip[] = "Lift moves from {$this->currentFloor}";

        if ($this->currentFloor !== $start) {
            $this->lastTrip[] = "Lift moves to {$start}";
        }

        if (!empty($floorList)) {
            $this->lastTrip[] = 'Lift stops at ' . implode(', ', $floorList);
        }

        $this->lastTrip[] = "Lift finish movement at {$end}";
        $this->currentFloor = $end;
    }
}
